<?php 

class Pendidikan extends CI_Controller{

	public function index()
	{
		$data['title'] = "Pendidikan";
		$data['pendidikan'] = $this->penggajianModel->get_data('pendidikan')
			->result();
		$this->load->view('templates_admin/header',$data);
		$this->load->view('templates_admin/sidebar');
		$this->load->view('admin/pendidikan',$data);
		$this->load->view('templates_admin/footer');
	}

	public function tambahData()
	{
		$data['title'] = "Tambah Data Pendidikan";
		$this->load->view('templates_admin/header',$data);
		$this->load->view('templates_admin/sidebar');
		$this->load->view('admin/tambahDataPendidikan',$data);
		$this->load->view('templates_admin/footer');
	}

	public function tambahDataAksi()
	{
		$this->_rules();

		if($this->form_validation->run() == FALSE) {
			$this->tambahData();
		}else{
			$p_terakhir			= $this->input->post('p_terakhir');
			$t_pendidikan		= $this->input->post('t_pendidikan');

			$data = array(

				'p_terakhir'	=> $p_terakhir,
				't_pendidikan'	=> $t_pendidikan,
		);

			$this->penggajianModel->insert_data($data, 'pendidikan');
			$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible fade show" role="alert">
		  <strong>Data berhasil ditambahkan!</strong>
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		  </button>
		</div>');
		redirect('admin/pendidikan');
		
		}
	}

	

	public function _rules()
	{
		$this->form_validation->set_rules('p_terakhir','p_terakhir','required');
		$this->form_validation->set_rules('t_pendidikan','t_pendidikan','required');
	}

	public function deleteData($id)
	{
		$where = array('pendidikan' => $id);
		$this->penggajianModel->delete_data($where, 'pendidikan');
		$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
		  <strong>Data berhasil dihapus!</strong>
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		  </button>
		</div>');
		redirect('admin/pendidikan');
	}
}

 ?>