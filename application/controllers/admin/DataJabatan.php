<?php 

class dataJabatan extends CI_Controller{

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('hak_akses') !='1') {
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
		  		<strong>Anda belum login!</strong>
		  		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  		<span aria-hidden="true">&times;</span>
		  		</button>
				</div>');
				redirect('welcome');
		}
	}

	public function index()
	{
		$data['title'] = "Data Jabatan";
		$data['jabatan'] = $this->penggajianModel->get_data('data_jabatan')
			->result();
		$this->load->view('templates_admin/header',$data);
		$this->load->view('templates_admin/sidebar');
		$this->load->view('admin/dataJabatan',$data);
		$this->load->view('templates_admin/footer');
	}

	public function tambahData()
	{
		$data['title'] = "Tambah Data Jabatan";
		$this->load->view('templates_admin/header',$data);
		$this->load->view('templates_admin/sidebar');
		$this->load->view('admin/tambahDataJabatan',$data);
		$this->load->view('templates_admin/footer');
	}

	public function tambahDataAksi()
	{
		$this->_rules();

		if($this->form_validation->run() == FALSE) {
			$this->tambahData();
		}else{
			$nama_jabatan		= $this->input->post('nama_jabatan');
			$gaji_pokok			= $this->input->post('gaji_pokok');
			$t_jabatan			= $this->input->post('t_jabatan');
			$t_anak				= $this->input->post('t_anak');
			$t_kesehatan		= $this->input->post('t_kesehatan');
			$t_pendidikan		= $this->input->post('t_pendidikan');
			$pajak				= $this->input->post('pajak');

			$data = array(

				'nama_jabatan'	=> $nama_jabatan,
				'gaji_pokok'	=> $gaji_pokok,
				't_jabatan'		=> $t_jabatan,
				't_anak'		=> $t_anak,
				't_kesehatan'	=> $t_kesehatan,
				't_pendidikan'	=> $t_pendidikan,
				'pajak'			=> $pajak,
		);

			$this->penggajianModel->insert_data($data, 'data_jabatan');
			$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible fade show" role="alert">
		  <strong>Data berhasil ditambahkan!</strong>
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		  </button>
		</div>');
		redirect('admin/dataJabatan');
		
		}
	}

	

	public function _rules()
	{
		$this->form_validation->set_rules('nama_jabatan','nama jabatan','required');
		$this->form_validation->set_rules('gaji_pokok','gaji pokok','required');
		$this->form_validation->set_rules('t_jabatan','t_jabatan','required');
		$this->form_validation->set_rules('t_anak','t_anak','required');
		$this->form_validation->set_rules('t_kesehatan','t_kesehatan','required');
		$this->form_validation->set_rules('t_pendidikan','t_pendidikan','required');
		$this->form_validation->set_rules('pajak','pajak','required');
	}

	public function deleteData($id)
	{
		$where = array('id_jabatan' => $id);
		$this->penggajianModel->delete_data($where, 'data_jabatan');
		$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
		  <strong>Data berhasil dihapus!</strong>
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		  </button>
		</div>');
		redirect('admin/dataJabatan');
	}
}

 ?>