<!DOCTYPE html>
<html lang="en">

<head>
    <title>Panduan User</title>
</head>

<body>
    <center>
        <!-- <iframe src="https://drive.google.com/file/d/193a3c_-EeLwsFcBL-o4N2dPCehj2Va8V/preview" width="1000" height="1000" allow="autoplay"></iframe> -->
        <div id="adobe-dc-view" style="height: 900px; width: 800px;"></div>

        <script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>

        <script type="text/javascript">
            document.addEventListener("adobe_dc_view_sdk.ready", function()

                {

                    var adobeDCView = new AdobeDC.View({
                        clientId: "0381239b56314756be7158d906a1636b",
                        divId: "adobe-dc-view"
                    });

                    adobeDCView.previewFile(

                        {

                            content: {
                                location: {
                                    url: "<?php echo base_url() ?>/assets/docs/User_Guide_Gajiku.pdf"
                                }
                            },

                            metaData: {
                                fileName: "User Guide Gajiku.pdf"
                            }

                        });

                });
        </script>
    </center>
</body>