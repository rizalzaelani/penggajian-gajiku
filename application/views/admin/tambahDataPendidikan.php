<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><?php echo $title ?></h1>
       
    </div>


    <div class="card" style="width: 60%; margin-bottom: 100px;">
        <div class="card-body">
            
            <form method="POST" action="<?php echo base_url('admin/pendidikan/tambahDataAksi') ?>">

                <div class="form-group">
                    <label>Pendidikan Terakhir</label>
                    <input type="text" name="p_terakhir" class="form-control">
                    <?php echo form_error('p_terakhir','<div class="text-small text-danger"></div>') ?>
                </div>

                <div class="form-group">
                    <label>Tunjangan Pendidikan</label>
                    <input type="text" name="t_pendidikan" class="form-control">
                    <?php echo form_error('t_pendidikan','<div class="text-small text-danger"></div>') ?>
                </div>
                
                

                <button type="submit" class="btn btn-info">Simpan</button>
            </form>
        </div>
    </div>

</div>
