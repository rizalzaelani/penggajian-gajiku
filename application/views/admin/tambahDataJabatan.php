<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><?php echo $title ?></h1>
       
    </div>


    <div class="card" style="width: 60%; margin-bottom: 100px;">
        <div class="card-body">
            
            <form method="POST" action="<?php echo base_url('admin/dataJabatan/tambahDataAksi') ?>">

                <div class="form-group">
                    <label>Nama Jabatan</label>
                    <input type="text" name="nama_jabatan" class="form-control">
                    <?php echo form_error('nama_jabatan','<div class="text-small text-danger"></div>') ?>
                </div>

                <div class="form-group">
                    <label>Gaji Pokok</label>
                    <input type="text" name="gaji_pokok" class="form-control">
                    <?php echo form_error('gaji_pokok','<div class="text-small text-danger"></div>') ?>
                </div>
                
                <div class="form-group">
                    <label>Tunjangan Jabatan</label>
                    <input type="text" name="t_jabatan" class="form-control">
                    <?php echo form_error('t_jabatan','<div class="text-small text-danger"></div>') ?>
                </div>

                <div class="form-group">
                    <label>Tunjangan Anak</label>
                    <input type="text" name="t_anak" class="form-control">
                    <?php echo form_error('t_anak','<div class="text-small text-danger"></div>') ?>
                </div>

                <div class="form-group">
                    <label>Tunjangan Kesehatan</label>
                    <input type="text" name="t_kesehatan" class="form-control">
                    <?php echo form_error('t_kesehatan','<div class="text-small text-danger"></div>') ?>
                </div>

                <div class="form-group">
                    <label>Tunjangan Pendidikan</label>
                    <input type="text" name="t_pendidikan" class="form-control">
                    <?php echo form_error('t_pendidikan','<div class="text-small text-danger"></div>') ?>
                </div>

                <div class="form-group">
                    <label>Pajak</label>
                    <input type="text" name="pajak" class="form-control">
                    <?php echo form_error('pajak','<div class="text-small text-danger"></div>') ?>
                </div>

                <button type="submit" class="btn btn-info">Simpan</button>
            </form>
        </div>
    </div>

</div>
