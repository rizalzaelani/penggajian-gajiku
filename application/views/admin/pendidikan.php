<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><?php echo $title ?></h1>
       
    </div>


    <?php echo $this->session->flashdata('pesan') ?>
    <table class="table table-bordered table-striped mt-2">
        <tr>
            <th class="text-center">No</th>
            <th class="text-center">Pendidikan Terakhir</th>
            <th class="text-center">Tunjangan Pendidikan</th>
        </tr>

        <?php $no=1; foreach($pendidikan as $p) : ?>
        <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $p->p_terakhir ?></td>
            <td><?php echo number_format($p->t_pendidikan, 0, ",", ".") ?></td>
        </tr>
    <?php endforeach; ?>

    </table>



</div>
