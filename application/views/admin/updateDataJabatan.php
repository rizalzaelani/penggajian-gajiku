<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><?php echo $title ?></h1>
       
    </div>


    <div class="card" style="width: 60%; margin-bottom: 100px;">
        <div class="card-body">

            <?php foreach ($jabatan as $j): ?>
            
            <form method="POST" action="<?php echo base_url('admin/dataJabatan/updateDataAksi') ?>">

                <div class="form-group">
                    <label>Nama Jabatan</label>
                     <input type="hidden" name="id_jabatan" class="form-control" value="<?php echo $j->id_jabatan ?>">
                    <input type="text" name="nama_jabatan" class="form-control" value="<?php echo $j->nama_jabatan ?>">
                    <?php echo form_error('nama_jabatan','<div class="text-small text-danger"></div>') ?>
                </div>

                <div class="form-group">
                    <label>Gaji Pokok</label>
                    <input type="number" name="gaji_pokok" class="form-control" value="<?php echo $j->gaji_pokok ?>">
                    <?php echo form_error('gaji_pokok','<div class="text-small text-danger"></div>') ?>
                </div>
                
                <div class="form-group">
                    <label>Tunjangan Jabatan</label>
                    <input type="number" name="t_jabatan" class="form-control" value="<?php echo $j->t_jabatan ?>">
                    <?php echo form_error('t_jabatan','<div class="text-small text-danger"></div>') ?>
                </div>

                <div class="form-group">
                    <label>Tunjangan Anak</label>
                    <input type="number" name="t_anak" class="form-control" value="<?php echo $j->t_anak ?>">
                    <?php echo form_error('t_anak','<div class="text-small text-danger"></div>') ?>
                </div>

                <div class="form-group">
                    <label>Tunjangan Kinerja</label>
                    <input type="number" name="t_kinerja" class="form-control" value="<?php echo $j->t_kinerja ?>">
                    <?php echo form_error('t_kinerja','<div class="text-small text-danger"></div>') ?>
                </div>

                <div class="form-group">
                    <label>Tunjangan Kesehatan</label>
                    <input type="number" name="t_kesehatan" class="form-control" value="<?php echo $j->t_kesehatan ?>">
                    <?php echo form_error('t_kesehatan','<div class="text-small text-danger"></div>') ?>
                </div>

                <button type="submit" class="btn btn-info">Update</button>
            </form>
        <?php endforeach ?>
        </div>
    </div>

</div>
